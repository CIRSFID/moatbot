import asyncio
import datetime
import logging
import random
import re

from aiogram import Bot, Dispatcher, executor, types

from moatbot.logging_utils import setup_logger
from moatbot.settings import (
    API_TOKEN, RPC_URL, CONTRACT_ADDRESS, ABI_PATH,
    NETWORK_NAME, LOG_FILEPATH, ALLOWED_GROUP_IDS, CHALLENGE_VOTING_WINDOW,
    FREE_ETHER, ONE_ETHER, INITIAL_COINS, BOT_ID
)
from .eth_interface import MOATcoin
from .helpers import HELP_MESSAGE
from .helpers import (
    new_candidate, candidate_exists, get_candidate, get_all_candidates, new_blame_message,
    get_blame_message, new_challenge, get_challenge, close_challenge, open_challenge,
    random_phrase, get_moat
)
from .helpers import parser

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

# setup logger
logger = setup_logger("moat", LOG_FILEPATH, logging.INFO, to_terminal=True, to_terminal_level=logging.INFO)

COOLDOWN_TABLE = {}

moat_obj = asyncio.run(get_moat())
moatcoin = MOATcoin(RPC_URL, CONTRACT_ADDRESS, ABI_PATH, moat_obj.account)


@dp.message_handler(commands=["help", "start"])
async def send_help(message: types.Message):
    await bot.send_message(message.chat.id, HELP_MESSAGE)


async def gotta_answer(message):
    if re.search(r"@(dev)?moat(bot)?", message.text, flags=re.I):
        return True
    if parser.search(message.text, "moat", "forbidden"):
        return True
    reply_to_message = message.values.get("reply_to_message")
    if reply_to_message is not None:
        if reply_to_message.poll:
            return True
        if re.search(r"@(dev)?moat(bot)?", reply_to_message.text, flags=re.I):
            return True
        if reply_to_message.from_user.first_name in ["devmoatbot", "moatbot"]:
            return True
    return False


@dp.message_handler(chat_id=ALLOWED_GROUP_IDS)
async def group_chat(message: types.Message):
    logger.info(f"New group message from {message.chat.id}: {message.from_user}\n\t{message.text}")
    if await gotta_answer(message):
        if await first_chat(message):
            await logic(message)
        return
    if random.random() <= 0.2:
        await bot.send_message(message.chat.id, await random_phrase())


@dp.message_handler(lambda message: types.chat.ChatType.is_private(message.chat))
async def private_chat(message: types.Message):
    logger.info(f"New message from: {message.from_user}\n\t{message.text}")
    if await first_chat(message):
        await logic(message)


@dp.poll_answer_handler()
async def poll_handler(poll_answer: types.PollAnswer):
    vote = poll_answer.values['option_ids'][0]
    # we switch them so they make sense with yes/no
    if vote == 0:
        in_favor = True
    elif vote == 1:
        in_favor = False
    else:
        raise Exception("Vote must be either 0 or 1")
    sender_id = poll_answer.user.id
    poll_id = poll_answer.poll_id
    voter_obj = await get_candidate(sender_id)
    voter = voter_obj.account
    challenge_obj = await get_challenge(poll_id=poll_id)
    chat_id = challenge_obj.chat_id
    tx_hash = moatcoin.vote(voter, challenge_obj.challenge_id, in_favor)
    # await bot.send_message(chat_id, f"Check your vote on etherscan: {etherscan_link(tx_hash)}")


async def first_chat(message: types.Message):
    telegram_id = message.from_user["id"]
    if not await candidate_exists(telegram_id):
        if types.chat.ChatType.is_private(message.chat):
            return await init_account(message)
        await message.reply("You are not registered yet. Contact me in private so I can set you up!")
        return False
    return True


async def init_account(message: types.Message):
    m = parser.search(message.text, "candidate")
    if m:
        candidate_key = m.group("candidate").first().key
        telegram_id = message.from_user["id"]
        candidate = await new_candidate(telegram_id=telegram_id, candidate_key=candidate_key)
        await bot.send_message(
            message.chat.id,
            "I'm working on it, it might take a while..."
        )
        moatcoin.mint(candidate.account.address, INITIAL_COINS)
        moatcoin.send_ether(moatcoin.master_account, candidate.account.address, FREE_ETHER)
        await bot.send_message(
            message.chat.id,
            f"Okay {m.value}, you are all set. "
            f"You were given {FREE_ETHER / ONE_ETHER} ETH and {INITIAL_COINS / ONE_ETHER} MOAT coins"
        )
        return True
    else:
        await bot.send_message(message.chat.id, f"Please enter your name so I can set up your wallet")
        return False


async def logic(message: types.Message):
    m = parser.search(message.text)
    sender_id = message.from_user["id"]
    if m:
        if m.type == "candidate":
            await bot.send_message(
                message.chat.id, await random_phrase()
                # f"Sorry, didn't get that. What did {await get_name(m.group('candidate').first().key)} do?"
            )
        elif m.type == "reveal_secret":
            if not types.chat.ChatType.is_private(message.chat):
                await bot.send_message(message.chat.id, "I cannot reveal your private key in a group chat!")
            else:
                await bot.send_message(
                    message.chat.id,
                    await get_candidate(message.from_user["id"]).private_key
                )
        elif m.type == "action":
            if message.chat.id not in ALLOWED_GROUP_IDS:
                await message.reply("You cannot do that outside of the LAST-JD chat group!")
                return
            action_group = m.first()
            candidate_group = action_group.group("candidate").first()
            candidate_key = candidate_group.key
            if not await candidate_exists(candidate_key):  # candidate is not registered
                await message.reply(f"{await get_name(candidate_key)} is not registered yet")
                return
            if action_group.key == "blame":
                n_beers = action_group.group("n_beers")
                if n_beers:
                    if n_beers.value.isdigit():
                        n = int(n_beers.value)
                    else:
                        n = 1
                else:
                    n = 1
                await blame(candidate_key, sender_id, message, n, candidate_group.value)
            if action_group.key == "paid":
                await paid(candidate_key, sender_id, message)
        elif m.type == "governance":
            gov_group = m.first()
            if gov_group.key == "challenge":
                rep = message.values.get("reply_to_message")
                if rep is not None:
                    blame_message = await get_blame_message(message.chat.id, rep.message_id)
                    if blame_message is not None:
                        await start_new_challenge(sender_id, message, blame_message)
            elif gov_group.key == "execute":
                poll_message = message.values.get("reply_to_message")
                if poll_message is not None and poll_message.poll:
                    challenge_obj = await get_challenge(poll_id=poll_message.poll.id)
                    if challenge_obj is not None:
                        await manualexec_challenge(sender_id, poll_message)
        elif m.type == "balance":
            sender_id = message.from_user["id"]
            first_group = m.first()
            if first_group.key == "balance":
                candidate_obj = await get_candidate(sender_id)
                address = candidate_obj.address
                token, ether = moatcoin.get_balance_of(address)
                stakes = len(moatcoin.get_stakes(address))
                await message.reply(
                    f"You have {int(token / ONE_ETHER)} MOAT tokens, {ether / ONE_ETHER:.2f} ETH "
                    f"and {stakes} beers to buy"
                )
            elif first_group.key == "beers":
                candidate = first_group.group("candidate")
                if candidate:
                    candidate_key = candidate.first().key
                    if not await candidate_exists(candidate_key):  # candidate is not registered
                        await message.reply(f"{await get_name(candidate_key)} is not registered yet")
                        return
                    candidate_obj = await get_candidate(candidate_key)
                    address = candidate_obj.address
                    opening = f"{await get_name(candidate_obj.candidate_key)} has"
                else:
                    candidate_obj = await get_candidate(sender_id)
                    address = candidate_obj.address
                    opening = f"You have"
                stakes = len(moatcoin.get_stakes(address))
                await message.reply(f"{opening} {stakes} beers to buy")
            elif first_group.key == "ranking":
                ranking = await get_ranking()
                strings = [
                    f"#{i + 1} {name} has to buy {n} beers" for i, (name, n) in enumerate(ranking)
                ]
                msg = "\n".join(strings)
                await message.reply(msg)
            else:
                raise Exception("Unknown query")
        elif m.type == "forbidden":
            await message.reply(f"Hey, you cannot say that!")
            culprit_obj = await get_candidate(sender_id)
            culprit_name = await get_name(culprit_obj.candidate_key)
            msg = f"@moatbot {culprit_name} said the word!"
            blame_message = await bot.send_message(
                message.chat.id, msg
            )
            await blame(culprit_obj.candidate_key, BOT_ID, blame_message, 1, culprit_name)

    else:
        text = await random_phrase()
        await bot.send_message(message.chat.id, text)  # f"Sorry, didn't get that")


async def blame(candidate_key, sender_id, message, n_beers, candidate_name):
    if can_be_blamed(candidate_key):
        await message.reply(
            f"Okay, I'm saving that on the blockchain. I'll tell you when it's done."
        )
        blamer = await get_candidate(sender_id)
        blamed = await get_candidate(candidate_key)
        try:
            for _ in range(0, n_beers):
                tx_hash, stake_id = moatcoin.stake_on(blamer.account, blamed.address)
                await new_blame_message(blamer, blamed, message.chat.id, message.message_id, stake_id, tx_hash)
                await message.reply(
                    f"{await get_name(sender_id)} just put a MOAT token at stake on {await get_name(candidate_key)}\n\n"
                    f"Check it on etherscan: {etherscan_link(tx_hash)}"
                )
        except Exception as e:
            if "revert" in f"{e}":
                await message.reply(
                    "You cannot do that!"
                )
            else:
                await message.reply(
                    f"Whoops! Something went wrong! {e}"
                )
    else:
        await message.reply(f"You cannot accuse {candidate_name} again yet!")


async def paid(candidate_key, sender_id, message):
    sender_obj = await get_candidate(candidate_key)
    if sender_id == sender_obj.telegram_id:
        await message.reply(f"Nice try, {await get_name(sender_id)}. Only someone else can testify on that!")
        return False
    await message.reply(
        f"Okay, I'm saving that on the blockchain. I'll tell you when it's done."
    )
    witness_obj = await get_candidate(sender_id)
    witness = witness_obj.account
    buyer_obj = await get_candidate(candidate_key)
    buyer = buyer_obj.address
    try:
        tx_hash = moatcoin.stake_won(witness, buyer)
        await message.reply(
            f"{await get_name(candidate_key)} paid a beer debt. Reward MOAT tokens were issued!\n\n"
            f"Check it on etherscan: {etherscan_link(tx_hash)}"
        )
    except Exception as e:
        if "revert" in f"{e}":
            await message.reply(
                "You cannot do that!"
            )
        else:
            await message.reply(
                f"Whoops! Something went wrong! {e}"
            )


async def start_new_challenge(sender_id, message, blame_message):
    if message.chat.id not in ALLOWED_GROUP_IDS:
        await message.reply("You cannot do that outside of the LAST-JD chat group!")
        return
    sender_name = await get_name(sender_id)
    stake_id = blame_message.stake_id
    defendant = await get_candidate(sender_id)
    blamed_id = blame_message.blamed_id
    if defendant.id != blamed_id:
        await message.reply(f"Only the one who was accused can challenge the accusation!")
        return
    challenge = await get_challenge(blame_message=blame_message)
    if challenge is not None:
        if challenge.is_open:
            await bot.send_message(message.chat.id, "A trial was already started on this stake!")
        else:
            await bot.send_message(message.chat.id, "A ruling was already issued on this stake!")
        return
    await message.reply(f"Okay, {sender_name}, let's vote!")
    try:
        tx_hash, challenge_id = moatcoin.new_challenge(defendant.account, stake_id)
    except Exception as e:
        await bot.send_message(message.chat.id, f"Whoops! Something went wrong: {e}")
        return
    poll_message = await bot.send_poll(
        chat_id=message.chat.id,
        question=f"Should {sender_name} buy a beer?",
        options=["Yes", "No"],
        is_anonymous=False
    )
    await bot.send_message(message.chat.id, f"Check the trial on etherscan: {etherscan_link(tx_hash)}")
    try:
        await new_challenge(
            defendant, message.chat.id, poll_message.message_id, poll_message.poll.id, challenge_id, blame_message,
            tx_hash
        )
    except Exception as e:
        await bot.send_message(message.chat.id, f"Whoops! Something went wrong: {e}")
        return
    await autoexec_challenge(poll_message, voting_window=CHALLENGE_VOTING_WINDOW)


async def autoexec_challenge(poll_message, voting_window):
    await asyncio.sleep(voting_window + 10)
    msg = f"The sentence was autoexecuted after {datetime.timedelta(seconds=voting_window)}. The verdict is "
    challenge = await close_challenge(poll_message.poll.id)
    if challenge is not None:  # the challenge was open and is now close
        try:
            tx_hash, result, yeas, nays = moatcoin.execute_challenge(moatcoin.master_account, challenge.challenge_id)
            msg += "GUILTY" if result else "NOT GUILTY"
            msg += f"\n\nTokens in favor: {int(yeas / ONE_ETHER)}\n\nTokens against: {int(nays / ONE_ETHER)}"
            msg += f"\n\nCheck it on etherscan {etherscan_link(tx_hash)}"
            await bot.stop_poll(poll_message.chat.id, poll_message.message_id)
            await poll_message.reply(msg)
        except Exception as e:
            await open_challenge(poll_message.poll.id)
            await bot.send_message(poll_message.chat.id, f"There was an error executing the challenge: {e}")
    else:
        await poll_message.reply(poll_message.chat.id, "The sentence was already closed")


async def manualexec_challenge(sender_id, poll_message):
    sender = await get_candidate(sender_id)
    challenge = await close_challenge(poll_message.poll.id)
    if challenge is not None:  # the challenge was open and is now close
        msg = f"The sentence was executed. The verdict is "
        try:
            tx_hash, result, yeas, nays = moatcoin.execute_challenge(sender.account, challenge.challenge_id)
            msg += "GUILTY" if result else "NOT GUILTY"
            msg += f"\n\nTokens in favor: {int(yeas / ONE_ETHER)}\n\nTokens against: {int(nays / ONE_ETHER)}"
            msg += f"\n\nCheck it on etherscan {etherscan_link(tx_hash)}"
            msg += f"\n\nCheck it on etherscan {etherscan_link(tx_hash)}"
            await bot.stop_poll(poll_message.chat.id, poll_message.message_id)
        except Exception as e:
            await open_challenge(poll_message.poll.id)
            await bot.send_message(poll_message.chat.id, f"There was an error executing the challenge: {e}")
    else:
        msg = "The sentence was already executed!"
    await poll_message.reply(msg)


def can_be_blamed(candidate_key):
    last_blamed = COOLDOWN_TABLE.get(candidate_key, datetime.datetime(1970, 1, 1))
    if datetime.datetime.now() - last_blamed < datetime.timedelta(seconds=120):
        return False
    COOLDOWN_TABLE[candidate_key] = datetime.datetime.now()
    return True


async def get_ranking():
    ranking = []
    candidates = await get_all_candidates()
    for candidate in candidates:
        if candidate.candidate_key != "moat":
            ranking.append((await get_name(candidate.candidate_key), len(moatcoin.get_stakes(candidate.address))))
    ranking.sort(key=lambda x: x[1], reverse=True)
    return ranking


def etherscan_link(tx_hash):
    if NETWORK_NAME == "mainnet":
        network_prefix = ""
    else:
        network_prefix = NETWORK_NAME + "."
    return f"https://{network_prefix}etherscan.io/tx/{tx_hash}"


async def get_name(candidate_key_or_telegram_id):
    def _key_to_name(_key):
        return " ".join(_key.split("_")).title()

    if isinstance(candidate_key_or_telegram_id, str):
        return _key_to_name(candidate_key_or_telegram_id)
    candidate = await get_candidate(candidate_key_or_telegram_id)
    return _key_to_name(candidate.candidate_key)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
    pass
