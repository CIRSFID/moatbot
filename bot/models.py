from django.db import models
from fernet_fields import EncryptedTextField
from web3.auto import w3

from moatbot.settings import NETWORK_ID


# Create your models here.
class Candidate(models.Model):
    telegram_id = models.IntegerField(db_index=True, unique=True)
    candidate_key = models.CharField(max_length=32, unique=True)
    private_key = EncryptedTextField()
    address = models.CharField(max_length=42, unique=True, db_index=True)
    last_update = models.DateTimeField(auto_now=True, db_index=True)

    @property
    def account(self):
        return w3.eth.account.privateKeyToAccount(self.private_key)

    def __str__(self):
        return f"{self.candidate_key} @ {self.account.address}"


class AbstractMessage(models.Model):
    chat_id = models.BigIntegerField(db_index=True)
    message_id = models.BigIntegerField(db_index=True)
    msg_timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        abstract = True
        unique_together = [("chat_id", "message_id")]


class AbstractTransaction(models.Model):
    network_id = models.IntegerField(default=NETWORK_ID, db_index=True)
    tx_hash = models.CharField(max_length=66, unique=True)

    class Meta:
        abstract = True
        unique_together = [("network_id", "tx_hash")]


class BlameMessage(AbstractMessage, AbstractTransaction):
    blamer = models.ForeignKey(Candidate, on_delete=models.CASCADE, related_name="blamer_candidate")
    blamed = models.ForeignKey(Candidate, on_delete=models.CASCADE, related_name="blamed_candidate")
    stake_id = models.IntegerField(db_index=True)

    def __str__(self):
        return f"{self.blamer.candidate_key} -> {self.blamed.candidate_key}"


class Challenge(AbstractMessage, AbstractTransaction):
    defendant = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    poll_id = models.BigIntegerField(db_index=True, unique=True)
    challenge_id = models.BigIntegerField(db_index=True, unique=True)
    blame_message = models.OneToOneField(BlameMessage, on_delete=models.CASCADE)
    is_open = models.BooleanField(default=True, db_index=True)

    def __str__(self):
        return f"Challange against {self.blame_message}"
