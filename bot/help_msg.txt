Welcome to MOATbot v2!

Through this bot we can interact with our MOATcoin Smart Contract to govern our game.

SETUP:
- Contact @moatbot in private to setup your account
- An Ethereum wallet will be created and managed for you
- 20 MOAT coins will be minted and given to you

HOW TO PLAY:
- If someone says a forbidden word outside of the workplace, they can be accused by someone else.
- To accuse: "@moatbot Max said the word". This action will put one of your tokens at stake.
- The accused candidate must pay a beer to pay their game debt.
- If the accused candidate pays their beer, someone else must testify on that.
- To testify: "@moatbot Max paid his beer". This action will cause: the staked token to return to the accuser; the minting of an award token for the accuser, the accused and the witness.
- If you are accused unjustly, you can start a trial.
- To challenge an accusation, REPLY to the message that accused you: "That is not true".
- The bot will create a POLL and every participant will be allowed to cast their vote for 2 hours.
- The votes will be saved on the blockchain.
- The votes are weighted: your vote weighs the number of tokens you have.
- The execution of the sentence will be automatic after 2 hours. If the trial is won, the accuser will lose their stake and the accused won't have to pay the beer.

COMMANDS:
- TO ACCUSE SOMEONE: "@moatbot Max said the word"
- TO TESTIFY: "@moatbot Max paid his beer"
- TO CHALLENGE: "That is not true" [REPLYING TO THE MESSAGE THAT ACCUSES YOU]
- TO VOTE: cast your vote clicking on the poll
- TO EXECUTE A SENTENCE: "execute" [REPLYING TO THE POLL, after 2 hours, if the sentence doesn't get autoexecuted for some reason]

WARNING: BEFORE VOTING ON A POLL, WAIT FOR THE TRIAL TO BE CONFIRMED ON THE BLOCKCHAIN, OTHERWISE YOUR VOTE WILL BE LOST FOREVER

BEWARE: If you say a forbidden word in the chat, you will be automatically accused by MOATbot!

There are other ways to trigger the commands, finding them is up to you!

HAVE FUN!