import datetime
import os
import random

import pytz
from channels.db import database_sync_to_async
from django.db.models import Max
from replus import Engine
from web3.auto import w3

from moatbot.settings import BOT_ID, MOAT_PRIVATE_KEY, NETWORK_ID
from .models import Candidate, BlameMessage, Challenge

__HERE__ = os.path.abspath(os.path.dirname(__file__))

PATTERNS_DIR = os.path.join(__HERE__, "patterns")
parser = Engine(PATTERNS_DIR, *"i")

with open(os.path.join(__HERE__, "help_msg.txt"), "r") as msg_file:
    HELP_MESSAGE = msg_file.read()


@database_sync_to_async
def new_candidate(telegram_id, candidate_key, private_key=None):
    if private_key is None:
        account = w3.eth.account.create(os.urandom(4096))
        private_key = account.privateKey.hex()
    else:
        account = w3.eth.account.privateKeyToAccount(private_key)
    candidate = Candidate.objects.create(
        telegram_id=telegram_id, candidate_key=candidate_key, private_key=private_key, address=account.address
    )
    candidate.save()
    return candidate


@database_sync_to_async
def candidate_exists(telegram_id_or_candidate_key):
    if isinstance(telegram_id_or_candidate_key, int):
        return Candidate.objects.filter(telegram_id=telegram_id_or_candidate_key).exists()
    elif isinstance(telegram_id_or_candidate_key, str):
        return Candidate.objects.filter(candidate_key=telegram_id_or_candidate_key).exists()
    else:
        raise TypeError(
            f"telegram_id_or_candidate_key must be either int or str: got {type(telegram_id_or_candidate_key)} instead"
        )


@database_sync_to_async
def get_candidate(telegram_id_or_candidate_key):
    if isinstance(telegram_id_or_candidate_key, int):
        try:
            return Candidate.objects.get(telegram_id=telegram_id_or_candidate_key)
        except Candidate.DoesNotExist:
            return None
    elif isinstance(telegram_id_or_candidate_key, str):
        try:
            return Candidate.objects.get(candidate_key=telegram_id_or_candidate_key)
        except Candidate.DoesNotExist:
            return None
    else:
        raise TypeError(
            f"telegram_id_or_candidate_key must be either int or str: got {type(telegram_id_or_candidate_key)} instead"
        )


@database_sync_to_async
def get_all_candidates(**kwargs):
    return list(Candidate.objects.all())


@database_sync_to_async
def new_blame_message(blamer, blamed, chat_id, message_id, stake_id, tx_hash, network_id=NETWORK_ID):
    blame_message = BlameMessage.objects.create(
        blamer=blamer, blamed=blamed, chat_id=chat_id, message_id=message_id,
        stake_id=stake_id, tx_hash=tx_hash, network_id=network_id
    )
    blame_message.save()
    return blame_message


@database_sync_to_async
def get_blame_message(chat_id, message_id):
    try:
        return BlameMessage.objects.get(chat_id=chat_id, message_id=message_id)
    except BlameMessage.DoesNotExist:
        return None


@database_sync_to_async
def new_challenge(defendant, chat_id, message_id, poll_id, challenge_id, blame_message, tx_hash,
                  network_id=NETWORK_ID):
    challenge = Challenge.objects.create(
        defendant=defendant, chat_id=chat_id, message_id=message_id, poll_id=poll_id, challenge_id=challenge_id,
        blame_message=blame_message, tx_hash=tx_hash, network_id=network_id
    )
    challenge.save()
    return challenge


@database_sync_to_async
def get_challenge(poll_id=None, challenge_id=None, blame_message=None):
    if poll_id:
        try:
            challenge = Challenge.objects.get(poll_id=poll_id)
            return challenge
        except Challenge.DoesNotExist:
            return None
    elif challenge_id:
        try:
            challenge = Challenge.objects.get(challenge_id=challenge_id)
            return challenge
        except Challenge.DoesNotExist:
            return None
    elif blame_message:
        try:
            challenge = Challenge.objects.get(blame_message=blame_message)
            return challenge
        except Challenge.DoesNotExist:
            return None
    raise Exception("You must provide either poll_id or challenge_id")


@database_sync_to_async
def close_challenge(poll_id):
    challenge = Challenge.objects.get(poll_id=poll_id)
    if challenge.is_open:
        challenge.is_open = False
        challenge.save()
        return challenge
    return None


@database_sync_to_async
def open_challenge(poll_id):
    challenge = Challenge.objects.get(poll_id=poll_id)
    if challenge.is_open:
        challenge.is_open = True
        challenge.save()
        return challenge
    return None


@database_sync_to_async
def random_phrase():
    phrases = [
        "Some day I'll be a conversational bot. Not today!",
        "I cannot really hold a conversation for now.",
        "My programmer hasn't finished me yet. Be patient, I'll be smart!",
        "Do you know that I'm a bot that can handle smart contracts?",
        "I really like the Ethereum blockchain."
    ]
    return random.choice(phrases)


@database_sync_to_async
def get_moat(private_key=None):
    try:
        return Candidate.objects.get(candidate_key="moat")
    except Candidate.DoesNotExist:
        private_key = private_key or MOAT_PRIVATE_KEY
        if private_key is None:
            while True:
                private_key = input("Please provide the MOAT master account private_key: ")
                try:
                    account = w3.eth.account.privateKeyToAccount(private_key)
                    break
                except ValueError:
                    print("The private key was invalid. Please provide a valid hex string.")
                except:
                    raise
        else:
            account = w3.eth.account.privateKeyToAccount(private_key)
        moat = Candidate.objects.create(
            candidate_key="moat", telegram_id=BOT_ID, private_key=private_key, address=account.address
        )
        moat.save()
        print(f"The MOAT master account was successfully created with address {moat.address}")
        return moat
