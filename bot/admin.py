from django.contrib import admin
from .models import Candidate, Challenge, BlameMessage

# Register your models here.
admin.site.register(Candidate)
admin.site.register(Challenge)
admin.site.register(BlameMessage)
