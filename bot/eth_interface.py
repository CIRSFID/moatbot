import json
from collections import defaultdict

import web3

ONE_ETHER = 1e18


class MOATcoin:

    def __init__(self, provider, contract_address, abi_path, master_account):
        self.w3 = web3.Web3(web3.Web3.HTTPProvider(provider))
        assert self.w3.isConnected(), "Could not connect to RPC"
        with open(abi_path) as f:
            info_json = json.load(f)
        abi_obj = info_json["abi"]
        self.contract = self.w3.eth.contract(contract_address, abi=abi_obj)
        self.master_account = master_account

    def _transact(self, sender, function_name, *args):
        nonce = self.w3.eth.getTransactionCount(sender.address)
        raw_tx = getattr(self.contract.functions, function_name)(
            *args
        ).buildTransaction({
            "from": sender.address,
            "nonce": nonce
        })
        private_key = sender.privateKey
        signed_txn = self.w3.eth.account.sign_transaction(raw_tx, private_key=private_key)
        tx_hash = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)
        return tx_hash.hex(), tx_receipt

    def mint(self, address: str, value):
        tx_hash, tx_receipt = self._transact(self.master_account, "mint", address, value)
        return tx_hash

    def stake_on(self, sender, receiver: str):
        tx_hash, tx_receipt = self._transact(sender, "stakeOn", receiver)
        stake_id = self.contract.events.Staked().processReceipt(tx_receipt)[0].args.id
        return tx_hash, stake_id

    def stake_won(self, sender, staked: str):
        tx_hash, tx_receipt = self._transact(sender, "stakeWon", staked)
        return tx_hash

    def new_challenge(self, sender, stake_id: int):
        tx_hash, tx_receipt = self._transact(sender, "newChallenge", stake_id)
        challenge_id = self.contract.events.ChallengeSubmitted().processReceipt(tx_receipt)[0].args.challengeID
        return tx_hash, challenge_id

    def vote(self, sender, challenge_id, in_favor):
        tx_hash, tx_receipt = self._transact(sender, "vote", challenge_id, in_favor)
        return tx_hash

    def remove_vote(self, sender, challenge_id):
        tx_hash, tx_receipt = self._transact(sender, "removeVote", challenge_id)
        return tx_hash

    def change_vote(self, sender, challenge_id, in_favor):
        tx_hash, tx_receipt = self._transact(sender, "changeVote", challenge_id, in_favor)
        return tx_hash

    def execute_challenge(self, sender, challenge_id):
        tx_hash, tx_receipt = self._transact(sender, "executeChallenge", challenge_id)
        result = self.contract.events.ChallengeExecuted().processReceipt(tx_receipt)[0].args
        return tx_hash, result.result, result.inFavour, result.against

    def get_all_stakes(self):
        tmp = defaultdict(list)
        for i in range(0, self.contract.functions._stakedNumber().call()):
            staked = self.contract.functions._staked(i).call()
            tmp[staked] = self.contract.functions.getStakesInfo(staked).call()
        return tmp

    def get_stakes(self, address: str):
        return self.contract.functions.getStakesInfo(address).call()

    def get_balance_of(self, address: str):
        return self.contract.functions.balanceOf(address).call(), \
               self.w3.eth.getBalance(address)

    def send_ether(self, from_account, to_address, amount):
        tx = {
            "from": from_account.address,
            "nonce": self.w3.eth.getTransactionCount(from_account.address),
            "to": to_address,
            "value": amount,
        }
        gas = self.w3.eth.estimateGas(tx)
        gas_price = self.w3.eth.gasPrice
        tx["gas"] = gas
        tx["gasPrice"] = gas_price
        signed_tx = from_account.signTransaction(tx)
        tx_hash = self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
        self.w3.eth.waitForTransactionReceipt(tx_hash)
        return tx_hash
