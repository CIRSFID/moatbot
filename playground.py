import asyncio
import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'moatbot.settings')
django.setup()

from bot.helpers import get_all_candidates
from moatbot.settings import DEBUG

if __name__ == '__main__':
    if not DEBUG:
        ans = input("You are in production. Do you wish to continue? ")
        if ans not in ["y", "yes"]:
            exit(0)
    _candidates = asyncio.run(get_all_candidates())
    for c in _candidates:
        vars()[c.candidate_key] = c
