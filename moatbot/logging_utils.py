import logging
import os
import sys


def setup_logger(name, log_file_path, level=logging.INFO, clear_file=False, to_terminal=False, to_terminal_level=None):
    log_dir, _ = os.path.split(log_file_path)
    os.makedirs(log_dir, exist_ok=True)

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    if clear_file:
        handler = logging.FileHandler(log_file_path, mode="w")
    else:
        handler = logging.FileHandler(log_file_path)

    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    if to_terminal:
        terminal_handler = logging.StreamHandler(sys.stdout)
        to_terminal_level = to_terminal_level if to_terminal_level is not None else level
        terminal_handler.setLevel(to_terminal_level)
        terminal_handler.setFormatter(formatter)
        logger.addHandler(terminal_handler)

    return logger
