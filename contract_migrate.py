import os
import django
import asyncio
import json
from tqdm import tqdm

tqdm.monitor_interval = 0

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'moatbot.settings')
django.setup()

from bot.telegram_bot import moatcoin
from bot.helpers import new_candidate
from moatbot.settings import ONE_ETHER, DEBUG, FREE_ETHER


async def import_data(skip_moat=True):
    with open("contract_data.json", "r") as f:
        data = json.load(f)
    for candidate_data in tqdm(data["candidates"]):
        if skip_moat and candidate_data["candidate_key"] == "moat":
            continue
        moat_balance = candidate_data.pop("moat_balance")
        candidate_data.pop("stakes")
        candidate_data.pop("address")
        candidate = await new_candidate(**candidate_data)
        challenges = data["challenges"].get(candidate.candidate_key, [])
        moat_balance += len(challenges) * int(ONE_ETHER)  # we need this to repeat the stakes
        moatcoin.mint(candidate.address, moat_balance)
        if DEBUG:
            moatcoin.send_ether(moatcoin.master_account, candidate.address, FREE_ETHER)
        account = candidate.account
        for challenged in challenges:
            tx_hash, stake_id = moatcoin.stake_on(account, challenged)
    print("\nAll good.")


if __name__ == '__main__':
    if not DEBUG:
        ans = input("You are in production. Do you wish to continue? ")
        if ans not in ["y", "yes"]:
            exit(0)
    asyncio.run(import_data(skip_moat=True))
